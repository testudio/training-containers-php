# Containers and continuous integration for PHP developers. 

TOC

Part 1. Introduction to containers. 
* [article](https://www.tomato-elephant-studio.com/book/containers-ci/1-containers)
* [code](https://gitlab.com/testudio/training-containers-php/tree/chapter-01)

Part 2. Composer and code sniffer (phpcs) tool. 
* [article](https://www.tomato-elephant-studio.com/book/containers-ci/2-composer)
* [code](https://gitlab.com/testudio/training-containers-php/tree/chapter-02)
