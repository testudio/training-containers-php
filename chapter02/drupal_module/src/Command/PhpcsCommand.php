<?php

// src/Command/ExpensesReindexCommand.php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;


class PhpcsCommand extends Command
{
// the name of the command (the part after "bin/console")
  protected static $defaultName = 'app:phpcs-example';

  protected function configure()
  {
    $this
      // the short description shown while running "php bin/console list"
      ->setDescription('Phpcs example.')
      // the full command description shown when running the command with
      // the "--help" option
      ->setHelp('Reindex expenses images and pdf files in specific folder.')
      ->addArgument('dir', InputArgument::REQUIRED, 'The directory');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    // outputs multiple lines to the console (adding "\n" at the end of each line)
    $output->writeln([
      'PHPCS symfony example',
      '============',
      '',
    ]);

    return [];
  }

}
